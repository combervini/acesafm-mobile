
const routes = [
  {
    path: '/',
    component: () => import('layouts/App.vue'),
    children: [
      { path: '', component: () => import('pages/Broadcast.vue') }
    ]
  },
  {
    path: '/order',
    component: () => import('layouts/App.vue'),
    children: [
      { path: '', component: () => import('pages/Order.vue') }
    ]
  },
  {
    path: '/schedules',
    component: () => import('layouts/App.vue'),
    children: [
      { path: '', component: () => import('pages/Order.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
