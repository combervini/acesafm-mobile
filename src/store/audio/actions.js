export const togglePlaying = (context) => {
  context.commit('audio/togglePlaying')
}

export const setAudio = (context) => {
  context.commit('setAudio', new Audio('http://radios.emid.com.br:9972/stream;'))
}
