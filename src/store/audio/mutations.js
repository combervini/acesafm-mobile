export const togglePlaying = (state) => {
  state.playing = !state.playing

  if (state.playing) state.audio.play()
  else state.audio.pause()
}

export const setVolume = (state, volume) => {
  state.volume = volume / 100

  state.audio.volume = state.volume
}

export const setAudio = (state, audio) => {
  if (state.audio) return
  state.audio = audio
}
