import Vue from 'vue'
// import VueCordova from 'vue-cordova'

import Vuex from 'vuex'
import audio from './audio'

// Vue.use(VueCordova)

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      audio
    }
  })

  return Store
}
